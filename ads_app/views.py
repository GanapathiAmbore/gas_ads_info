from django.shortcuts import render
from ads_app.models import AdsData
from django.db.models import Count
from django.core.paginator import Paginator,PageNotAnInteger,EmptyPage
from django.views.generic import DetailView
def home(request):
    users=AdsData.objects.all()[0:10]
    data_count=AdsData.objects.count()
    country_count=AdsData.objects.values('country').annotate(the_count=Count('country'))
    media_count = AdsData.objects.values('media_type').annotate(the_count=Count('media_type'))

    page = request.GET.get('page', 10)

    paginator = Paginator(users, 3)
    try:
        users = paginator.page(page)
    except PageNotAnInteger:
        users = paginator.page(1)
    except EmptyPage:
        users = paginator.page(paginator.num_pages)
    return render(request,'home.html',{'users':users,'data_count':data_count,'country_count':country_count,'media_count':media_count})

def index(request):
    adsdata = AdsData.objects.all()
    source_list = adsdata.values('source').distinct()[:100]
    comany_list = adsdata.values('master_company').distinct()[:100]
    media_list = adsdata.values('media_type').distinct()[:100]
    brand_list = adsdata.values('master_brand').distinct()[:100]
    category_list = adsdata.values('master_category').distinct()[:100]
    country_list = adsdata.values('country').distinct()[:100]
    users=[]
    if request.GET.get('master_category'):
        category = request.GET.get('master_category',None)
        source = request.GET.get('source',None)
        master_company = request.GET.get('master_company',None)
        master_brand = request.GET.get('master_brand',None)
        media_type = request.GET.get('media_type',None)
        country = request.GET.get('country',None)
        if category and source and master_brand and master_company and media_type and country:
            users = AdsData.objects.filter(master_category=category,country=country,media_type=media_type,master_brand=master_brand)[0:100]
            print(users,"users")
        else:
            print("hiiiiiiiiiiiiiiiii")
    else:
        print("jjjjjjjjjjjjjjjjjjjjjj")
        users = AdsData.objects.all()[0:100]



    page = request.GET.get('page', 1)

    paginator = Paginator(users, 5)
    try:
        users = paginator.page(page)
    except PageNotAnInteger:
        users = paginator.page(1)
    except EmptyPage:
        users = paginator.page(paginator.num_pages)
    return render(request,'index.html',{'users':users,"source_list":source_list,"comany_list":comany_list,"media_list":media_list,"brand_list":brand_list,"category_list":category_list,"country_list":country_list})

def detail(request,data_id):
    users = AdsData.objects.get(data_id=data_id)
    return render(request,'detail.html',{'val':users})